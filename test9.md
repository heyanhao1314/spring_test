

本地普通java项目转为maven项目：
   1：右键项目 add Frameworkwork Support ->选择maven即可！ 


服务器端修改默认项目分支：
操作步骤如：
    1：登录gitlab管理界面
    2：进入需要修改的项目，settings -> repository
    3：右边操作界面中查找 Default branch
    4：展开Default branch，可以看到修改默认分支！



删除git分支方式：

一：gitlib服务器端删除：
   服务器端进到需要删除分支的项目 -> Repository -> Branches -> 可以查看到该项目下所有的分支，删除图标删除即可！
   (如果是默认分支无法删除，进行上面默认分支的修改！
    此时还不能删除该分支，因为之前该默认分支处于保护状态，此时可以操作如下：
    一：settings -> repository -> Protected branches  展开该选项可以看到
        Unpprotect  即为删除保护状态
        (settings -> General 此为修改git上面项目名称)


二：本地客户端删除：命令方式删除:  如果无法删除：参考步骤一注意点；
   1：git push origin :xxx  (xxx为需要删除的远程分支)
    
   2：git branch -d xxx 删除本地分支{远程分支没有，本地创建的分支需要删除，使用此命令}
   
   3：git push origin --delete xxx 删除远程分支


IDEA如何先把本地修改放到暂存区：
 
  1：Git -> Repository -> stash  Changes（加到暂存区）   （unStash  Changes放开暂存区）

  2：将暂存区提交到仓库区(暂存区 --> 仓库区)
      格式: git commit -m '注释'
      
  3：工作区 -->仓库区 (直接将工作区的变动, 提交到仓库区)
      格式: git commit -am '注释信息'
      
        
    

三：回退版本:
    格式1: git reset --hard HEAD^
    解释:
        HEAD    表示当前最新版本
        HEAD^   表示当前最新版本的,  上一个版本
        HEAD^^  表示当前最新版本的, 前两个版本, 依次类推
        HEAD~1  表示当前最新版本的,  上一个版本
        HEAD~2  表示当前最新版本的, 前两个版本, 依次类推

   
   

   
   


